#!/bin/bash

function change_context
{
    context=$1
    echo "switching to ${context}"
    kubectl config set-context --current --namespace=${context}
}

function change_to_cluster_instance
{
    clustername=$1
    echo "changing to ${clustername}"
    ln -fs ~/.kube/config.${clustername} ~/.kube/config
    echo
    kubectl get nodes
}

function list_clusters
{
    echo "Current namespace@cluster:"
    cluster=$(readlink ~/.kube/config | sed 's/^.*config.//')
    namespace=$(kubectl config view --minify --output 'jsonpath={..namespace}'; echo)
    echo "${namespace}@${cluster}"
    echo
    echo "Following configs found:"
    for clu in $(ls ~/.kube/config.*)
    do
        basename ${clu} | sed 's/config.//'
    done
}

function usage
{
    echo "USAGE: $0"
    echo "  -h .. help"
    echo "  -l .. list cluster configs"
    echo "  -i <cluster> .. switch config to <cluster instace>"
    echo "  -c <cluster> .. switch config to <context>"
    exit 1
}

while getopts c:i:hl flag
do
    case "${flag}" in
        h)
            usage
            ;;
        l)
            list_clusters
            ;;
        i)
            clustername=${OPTARG}
            change_to_cluster_instance ${clustername}
            ;;
        c)
            contextname=${OPTARG}
            change_context ${contextname}
            ;;
    esac
done

if [ -z $1 ];
then
    list_clusters
fi
