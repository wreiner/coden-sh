#!/bin/sh

DIR=$1

if [ "$DIR" == "" ]
then
	echo "please provide a directory we should work in"
	exit -1
fi

if [ ! -d $DIR ] 
then 
	echo "directory $DIR is not a directory"
	exit -1
fi

cd $DIR
PWD=`pwd`

if [ "$PWD" != "$DIR" ]
then
	echo "pwd is not $DIR - we are in $PWD"
	exit -1
fi

for i in `ls $DIR`
do
	echo "$i"
	len=${#i}
	let "len -= 4"
	file=${i:0:$len}

	mplayer -vo null -vc dummy -af resample=44100 -ao pcm $i && lame -m s audiodump.wav -o $file.mp3

	if [ -f audiodump.wav ]
	then
		rm audiodump.wav
	fi
done
