#!/bin/sh

startdir="/home/vmail"

for xx in "/home/vmail" "/var/" "/home" "/usr/local/" "/usr/src"
do
	for dir in `find $xx -maxdepth 1 -type d`
	do
		space=`du -ksh "$dir" | awk '{print $1}'`
		echo "$dir: $space"
	done
done
