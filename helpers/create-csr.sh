#!/bin/sh
#
# Example CSR config file:
#
# [req]
# default_bits       = 2048
# distinguished_name = dn
# prompt             = no
# req_extensions     = req_ext
#
# [dn]
# C="AT"
# ST="Styria"
# L="Graz"
# O="COMPANY LTD"
# OU="Department"
# emailAddress="hostmaster@company.tld"
# CN="<hostidentifier-fqdn>"
#
# [req_ext]
# subjectAltName = @alt_names
#
# [alt_names]
# DNS.0 = *.yourdomain.com
# DNS.1 = *.dev.yourdomain.com
#

system=$1
csrconfig=$2

if [ -z ${system} ] || [ -z ${csrconfig} ];
then
    echo "USAGE: $0 <systemname> <csr-config-file>"
    exit 1
fi

echo "Creating CSR .."
openssl req -out ${system}.csr -newkey rsa:2048 -nodes -keyout ${system}.key -config ${csrconfig}

echo "Please check the CSR:"
openssl req -text -noout -verify -in ${system}.csr
