#!/bin/bash
#
# This script should be used to record voice notes for studying.
#
# It uses ffmpeg and lame encoder.
#   ffmpeg -f alsa -i pulse -codec:a libmp3lame -qscale:a 2 "$(date "+%d%m%Y-%H%M%S")_FamR.mp3"
#
#   The codec parameters can be seen here:
#       https://trac.ffmpeg.org/wiki/Encode/MP3
#
#       -qscale:a 2 === VBR Encoding, Average kbit/s 190, Bitrate range kbit/s 170-210 
#
# Walter Reiner <walter.reiner@wreiner.at>
# December 2015
#

function usage()
{
    echo "USAGE: $0 <identifier> [<title>]"
    echo
    echo "  <identifier>   should be the class, field, .."
    echo "  <title>        a simple title"
    echo
    exit 1
}

# basic config
REC_BASE_DIR="/mnt/dl"
TIMESTRING=$(date "+%Y%m%d-%H%M%S")
FFMPEG_BIN="/usr/bin/ffmpeg"
#FFMPEG_FLAGS="-f alsa -i pulse -codec:a libmp3lame"
FFMPEG_FLAGS="-f alsa -i pulse -acodec mp3 -ab 128k"

# get class, field, ..
ident=$1

if [ -z $ident ]
then
    usage
    exit 1
fi

title=$2

if [ -z $title ]
then
    title=""
fi

# all values used should be set from this point on
set -o nounset
# exit on error
set -o errexit

# create subdir if needed
if [ ! -d "$REC_BASE_DIR/$ident" ]
then
    mkdir -p "$REC_BASE_DIR/$ident"
fi

outfile=""
if [ -z $title ]
then
    outfile="$REC_BASE_DIR/$ident/${TIMESTRING}_${ident}.mp3"
else
    outfile="$REC_BASE_DIR/$ident/${TIMESTRING}_${ident}_${title}.mp3"
fi
echo "will record to file[$outfile] .."
echo "----- To stop recording use STRG+C!"
echo
$FFMPEG_BIN $FFMPEG_FLAGS $outfile
