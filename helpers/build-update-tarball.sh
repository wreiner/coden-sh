#!/bin/sh
#
# This script creates a tarball with all files that were changed
# between two Git commits.
#
# Walter Reiner <walter.reiner@wreiner.at> 2017
#

function usage()
{
    echo "USAGE: $0 <tarball-identifier> <git-sha-last-release> <git-sha-current-release>"
    echo
    echo "  Run this script inside of your Git repository."
    echo
}

# https://stackoverflow.com/a/31348007/7523861
if ! git status >> /dev/null 2>&1;
then
    usage
    exit 1
fi

if [ -z $1 ] || [ -z $2 ] || [ -z $3 ];
then
    usage
    exit 1
fi

TARBALL_ID=$1
FROM_GIT_COMMIT=$2
TO_GIT_COMMIT=$3

DEPLOY_TARBALL="deploy/$(date "+%Y%m%d-%H%M%S")_$TARBALL_ID.tar"

RELEASE_FILES=$(git diff --name-only $FROM_GIT_COMMIT $TO_GIT_COMMIT)

echo "Generating deploy tarball .."
tar cf $DEPLOY_TARBALL --files-from /dev/null
for f in $RELEASE_FILES
do
    echo "adding [$f] to deploy tarball .."
    tar rf $DEPLOY_TARBALL "$f"
done
bzip2 $DEPLOY_TARBALL
echo "Done."

echo
echo
echo "DEPLOY TARBALL:   $DEPLOY_TARBALL.bz2"
echo
