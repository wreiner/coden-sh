#!/bin/sh
#
# This script searches for the Minecraft 1.13 window
# brings it into foreground and focus and starts right
# clicking every 100ms.
# If the focus is changed to another window, the script
# exits.
#
# This is used for the AFK fish farms in Minecraft.
#
# Note:
#   Install xdotool for it to work.
#
# Walter Reiner 2018 <walter.reiner@wreiner.at>
#

WINDOW_ID=$(xdotool search --name "^Minecraft 1.13$")
if [ ! -z $WINDOW_ID ];
then
    echo "getting window into focus .."
    xdotool windowactivate $WINDOW_ID 
    xdotool windowfocus $WINDOW_ID 
    sleep 5
    while true
    do
        if [ $WINDOW_ID != $(xdotool getwindowfocus) ];
        then
            echo "lost window focus, exiting .."
            exit 1
        fi

        echo "clicking .."
        xdotool click 3
        sleep 0.1
    done
fi

echo "done."
