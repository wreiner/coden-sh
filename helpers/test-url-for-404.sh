#!/bin/bash
#
# This script retrieves the HTTP status code from a list of urls
# supplied via STDIN.
#
# To test all urls in the elektronik/HELPERS file run:
#   grep -i http HELPERS | sed -e 's/.*\(http:.*\)/\1/' | awk '{print $1;}' | $0
#
# Walter Reiner <walter.reiner@wreiner.at>
# December 2015

while read url
do
    echo -n "$url ... "
    http_status=$(curl -m 15 -s -o /dev/null -I -w "%{http_code}" "$url")
    echo $http_status
done
