#!/bin/bash

function logging
{
    APP=$(basename $0)
    echo "$(date '+%Y-%m-%d %H:%M:%S') ${APP} $1"
}
