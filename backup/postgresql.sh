
function backup_postgresql
{
    [ -v ${POSTGRESQLBACKUP_ACTIVE} ] && POSTGRESQLBACKUP_ACTIVE=0
    if [ ${POSTGRESQLBACKUP_ACTIVE} -ne 1 ];
    then
        logging "postgresqlbackup not active."
        return
    fi

    logging "postgresql backup started .."

    logging "will backup to ${BACKUP_DEST_DIR}/psql.sql.gz .."
    ${SUDO_BIN} -i -u postgres ${PGDUMPALL_BIN} | gzip > ${BACKUP_DEST_DIR}/psql.sql.gz

    logging "postgresql backup finished."
}
