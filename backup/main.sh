#!/usr/bin/env bash

INSTALL_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

. ${INSTALL_DIR}/$(hostname)-config.sh
. ${INSTALL_DIR}/base.sh
. ${INSTALL_DIR}/mysql.sh
. ${INSTALL_DIR}/postgresql.sh
. ${INSTALL_DIR}/debian.sh
. ${INSTALL_DIR}/borg.sh

mkdir -p ${BACKUP_DEST_DIR}

logging "# -- BACKUP STARTED"
backup_mysql
debian_installed_packages
borg_backup
logging "# -- BACKUP finished."
