
function borg_backup
{
    logging "borg_backup started .."

    export BORG_PASSPHRASE=${BORG_PASSPHRASE}

    for borg_host in ${BORG_HOSTS}
    do
        ip=${borg_host#*:}
        dirpart=${borg_host%:*}
        borg_remote_path="/mnt/backup-${dirpart}/$(hostname).borg"

        logging "will backup to ${borg_host} .."
        ${BORG_BIN} create --progress --stats --verbose --list --filter AME ${BORG_USER}@${ip}:${borg_remote_path}::$(date '+%Y%m%d%H%M%S') ${BACKUP_DEST_DIR} ${DIRECTORIES_TO_BACKUP} ${DIRECTORIES_TO_EXCLUDE} >> ${BORG_LOGFILE} 2>&1
    done

    logging "borg_backup finished."
}
