
function backup_mysql
{
    [ -v ${MYSQLBACKUP_ACTIVE} ] && MYSQLBACKUP_ACTIVE=0
    if [ ${MYSQLBACKUP_ACTIVE} -ne 1 ];
    then
        logging "mysqlbackup not active."
        return
    fi

    if [ -z ${MYSQL_HOST} ];
    then
        MYSQL_HOST="127.0.0.1"
    fi

    if [ -z ${MYSQL_PORT} ];
    then
        MYSQL_PORT="3306"
    fi

    logging "mysql backup started .."

    for DB in ${MYSQL_DBS}
    do
        logging "will backup to ${BACKUP_DEST_DIR}/${DB}.sql .."

        ${MYSQLDUMP_BIN} -u ${MYSQL_USER} -p${MYSQL_PASS} -h ${MYSQL_HOST} -P ${MYSQL_PORT} ${DB} | gzip > ${BACKUP_DEST_DIR}/${DB}.sql.gz
    done

    logging "mysql backup finished."
}
