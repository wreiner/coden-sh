
# -- global config
BACKUP_DEST_DIR="/tmp/$(hostname)"
SUDO_BIN="/usr/bin/sudo"

# -- borg settings
BORG_BIN="/usr/bin/borg"
BORG_HOSTS="dirpath:192.168.122.224"
BORG_USER="rbackup"
BORG_PASSPHRASE="1234"
BORG_LOGFILE="/home/wreiner/backup/borg.log"

# -- mysql settings
#
# To create a backup user run:
#   CREATE USER 'backup'@'localhost' IDENTIFIED BY 'secret';
#   GRANT SELECT, SHOW VIEW, RELOAD, REPLICATION CLIENT, EVENT, TRIGGER ON *.* TO 'backup'@'localhost';
#   GRANT LOCK TABLES ON *.* TO 'backup'@'localhost';
#
MYSQLBACKUP_ACTIVE=1
MYSQLDUMP_BIN="/usr/bin/mysqldump"
MYSQL_USER="backup"
MYSQL_PASS="secret"
MYSQL_HOST="127.0.0.1"
MYSQL_PORT="3306"
MYSQL_DBS="mysql dbA dbB"

# -- postgresql settings
POSTGRESQLBACKUP_ACTIVE=1
PGDUMPALL_BIN="/usr/bin/pg_dumpall"

# -- backup directories
DIRECTORIES_TO_BACKUP="/home /opt/testitest"
DIRECTORIES_TO_EXCLUDE="--exclude /home/wreiner/backup"
