##
## Create list of installed software
##

function debian_installed_packages
{
    logging "generating installed package list .."
    dpkg --get-selections > ${BACKUP_DEST_DIR}/dpkg.list
}
